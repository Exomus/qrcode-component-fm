# Frontend Mentor - QR code component solution

This is a solution to the [QR code component challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/qr-code-component-iux_sIO_H). Frontend Mentor challenges help you improve your coding skills by building realistic projects. 

## Table of contents

- [Overview](#overview)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [What I learned](#what-i-learned)
  - [Continued development](#continued-development)
  - [Useful resources](#useful-resources)
- [Author](#author)
- [Acknowledgments](#acknowledgments)

**Note: Delete this note and update the table of contents based on what sections you keep.**

## Overview

### Screenshot

![Screenshot of the challenge](assets/screenshot.png)

### Links

- Solution URL: [Solution URL](https://qrcode-component-fm.vercel.app/)

## My process

### Built with

- Semantic HTML5 markup
- CSS Media queries
- Flexbox

### What I learned

I refreshed my knowledge about media queries in a raw CSS context.

### Continued development

I want to expand my knowledge about responsive designs and gain in speed to achieve the styling.

### Useful resources

- [A code pen with some HTML5 structure for cards](https://codepen.io/mikedidthis/pen/PNYjPN) - This reminded me the HTML5 semantic elements

## Author

- Frontend Mentor - [@Exomus](https://www.frontendmentor.io/profile/Exomus)
- Twitter - [@axiom_3d](https://www.twitter.com/axiom_3d)
